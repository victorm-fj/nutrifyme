import { combineReducers } from 'redux';
import profile from './ProfileReducer';
import storage from './StorageReducer';

export default combineReducers({
  profile,
  storage
});
