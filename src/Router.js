import React from 'react';
import { StyleSheet } from 'react-native';
import { Scene, Router, Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import Home from './components/main/Home';
import Stats from './components/main/Stats';
import Diary from './components/main/Diary';
import Profile from './components/main/Profile';
import Intro from './components/intro/Intro';
import PartialResults from './components/PartialResults';
import Goal from './components/Goal';

const TabIcon = ({ selected, title }) => {
  return (
    <Icon name={`md-${title}`} size={22} color={ selected ? '#774DC6' : '#8dd8d8' } />
  );
}

const RouterComponent = () => {
  return (
    <Router navigationBarStyle={styles.navBarStyle}>

      <Scene key="intro">
        <Scene key="swiper" component={Intro} hideNavBar={true} />
      </Scene>

      <Scene key="result">
        <Scene key="imc" component={PartialResults} hideNavBar={true} />
        <Scene key="goal" component={Goal} hideNavBar={true} />
      </Scene>

      <Scene key="root">
        {/* Tab Container */}
        <Scene key="tabBar"
               tabs={true}
               tabBarStyle={styles.tabBarStyle}>
          {/* Home Tab and its scenes */}
          <Scene key="home"
                 title="home"
                 icon={TabIcon}>
            <Scene key="homeComponent"
                   component={Home}
                   title="Home"
                   titleStyle={{ color: '#fff' }} />
          </Scene>
          {/* Diary Tab and its scenes */}
          <Scene key="diary"
                 title="calendar"
                 icon={TabIcon}>
            <Scene key="diaryComponent"
                   component={Diary}
                   title="Diary"
                   titleStyle={{ color: '#fff' }} />
          </Scene>
          {/* Home Tab and its scenes */}
          <Scene key="stats"
                 title="stats"
                 icon={TabIcon}>
            <Scene key="statsComponent"
                   component={Stats}
                   title="Stats"
                   titleStyle={{ color: '#fff' }} />
          </Scene>
          {/* Home Tab and its scenes */}
          <Scene key="profile"
                 title="person"
                 icon={TabIcon}>
            <Scene key="profileComponent"
                   component={Profile}
                   title="Profile"
                   titleStyle={{ color: '#fff' }} />
          </Scene>
        </Scene>
      </Scene>

    </Router>
  );
}

const styles = StyleSheet.create({
  navBarStyle: {
    backgroundColor: '#3FB8B8'
  },
  tabBarStyle:{
    backgroundColor: '#3FB8B8',
    height: 56
  }
});

export default RouterComponent;
