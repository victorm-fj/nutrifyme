import { persistStore, autoRehydrate } from 'redux-persist';
import { applyMiddleware, createStore, compose } from 'redux';
import { AsyncStorage } from 'react-native';
import ReduxThunk from 'redux-thunk';
import createLogger from 'redux-logger';
import reducers from './reducers';

const logger = createLogger();

const store = createStore(reducers, compose(
  applyMiddleware(ReduxThunk, logger),
  autoRehydrate()
));

persistStore(store, { storage: AsyncStorage });

export default store;
