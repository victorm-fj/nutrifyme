import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

class Stats extends Component {
  render() {
    return (
      <View style={styles.containerStyle}>
        <Text style={styles.textStyle}>
          This is the Stats component.
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle: {
    fontSize: 20,
  }
});

export default Stats;
