import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

class Profile extends Component {
  render() {
    return (
      <View style={styles.containerStyle}>
        <Text style={styles.textStyle}>
          This is the Profile component.
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle: {
    fontSize: 20,
  }
});

export default Profile;
