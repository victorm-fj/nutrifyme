import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button } from 'react-native-elements';

class Home extends Component {
  render() {
    return (
      <View style={styles.container}>

        <View style={[styles.wrapper, { flex: 2, paddingHorizontal: 15 }]}>
        </View>

        <View style={[styles.wrapper, { flex: 1, flexDirection: 'row', paddingHorizontal: 15 }]}>
          <View style={{ flex: 1, backgroundColor: '#00B0FF', height: 60 }}>

          </View>

          <View style={{ flex: 1, backgroundColor: '#00B200', height: 60 }}>

          </View>

          <View style={{ flex: 1, backgroundColor: '#430280', height: 60 }}>

          </View>

          <View style={{ flex: 1, backgroundColor: '#F4B800', height: 60 }}>

          </View>

          <View style={{ flex: 1, backgroundColor: '#FF7900', height: 60 }}>

          </View>

          <View style={{ flex: 1, backgroundColor: '#E50405', height: 60 }}>

          </View>
        </View>

        <View style={[styles.wrapper, { flex: 0.75 }]}>
          <Button small
                  raised
                  title="Continuar"
                  backgroundColor="#774DC6"
                  onPress={() => Actions.goal()} />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 56
  },
  wrapper: {
    justifyContent: 'space-around',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#212121'
  },
  titleStyle: {
    fontSize: 22,
    color: '#212121'
  },
  primaryText: {
    fontSize: 16,
    color: '#212121',
    textAlign: 'center'
  }
});

export default Home;
