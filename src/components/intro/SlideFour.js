import React from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { CheckBox, Button } from 'react-native-elements';

const SlideFour = () => {
  return (
    <View style={styles.container}>

      <View style={[styles.wrapper, { flex: 0.5, paddingHorizontal: 15 }]}>
        <Text style={styles.primaryText}>
          Por último, elige el número de comidas que haces habitualmente
          en un día:
        </Text>
      </View>

<<<<<<< HEAD
      <View style={[styles.wrapper, { flex: 3 }]}>
        <View style={styles.inputGroup}>
          <CheckBox center
                    title="3 comidas"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checked={true}
                    checkedColor="#774DC6"
                    uncheckedColor="#fff"
                    containerStyle={styles.checkBox}
                    textStyle={{ color: '#fff' }} />
          <CheckBox center
                    title="4 comidas"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checked={false}
                    checkedColor="#774DC6"
                    uncheckedColor="#fff"
                    containerStyle={styles.checkBox}
                    textStyle={{ color: '#fff' }} />
          <CheckBox center
                    title="5 comidas"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checked={false}
                    checkedColor="#774DC6"
                    uncheckedColor="#fff"
                    containerStyle={styles.checkBox}
                    textStyle={{ color: '#fff' }} />
        </View>
||||||| merged common ancestors
      <View style={styles.inputGroupStyle}>
        <CheckBox center
                  title="3 comidas"
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  checked={true}
                  checkedColor="#774DC6"
                  uncheckedColor="#fff"
                  containerStyle={styles.checkBoxStyle}
                  textStyle={{ color: '#fff' }} />
        <CheckBox center
                  title="4 comidas"
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  checked={false}
                  checkedColor="#774DC6"
                  uncheckedColor="#fff"
                  containerStyle={styles.checkBoxStyle}
                  textStyle={{ color: '#fff' }} />
        <CheckBox center
                  title="5 comidas"
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  checked={false}
                  checkedColor="#774DC6"
                  uncheckedColor="#fff"
                  containerStyle={styles.checkBoxStyle}
                  textStyle={{ color: '#fff' }} />
=======
      <View style={[styles.wrapper, { flex: 3 }]}>
        <View style={styles.inputGroupStyle}>
          <CheckBox center
                    title="3 comidas"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checked={true}
                    checkedColor="#774DC6"
                    uncheckedColor="#fff"
                    containerStyle={styles.checkBoxStyle}
                    textStyle={{ color: '#fff' }} />
          <CheckBox center
                    title="4 comidas"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checked={false}
                    checkedColor="#774DC6"
                    uncheckedColor="#fff"
                    containerStyle={styles.checkBoxStyle}
                    textStyle={{ color: '#fff' }} />
          <CheckBox center
                    title="5 comidas"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checked={false}
                    checkedColor="#774DC6"
                    uncheckedColor="#fff"
                    containerStyle={styles.checkBoxStyle}
                    textStyle={{ color: '#fff' }} />
        </View>
>>>>>>> e4dca620ee9e714d44941a051d2df5cf5c909469
      </View>
<<<<<<< HEAD
      {/* justifyContent: 'flex-start', looks good in emulator but not in actual device */}
      <View style={[styles.wrapper, { flex: 0.75 }]}>
||||||| merged common ancestors

      <View style={styles.wrapper}>
=======

      <View style={[styles.wrapper, { flex: 0.75, justifyContent: 'flex-start' }]}>
>>>>>>> e4dca620ee9e714d44941a051d2df5cf5c909469
        <Button small
                raised
                title="Continuar"
                backgroundColor="#774DC6"
                onPress={() => Actions.result()} />
      </View>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3FB8B8'
  },
  wrapper: {
    justifyContent: 'space-around',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#fff'
  },
  primaryText: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center'
  },
<<<<<<< HEAD
  secondaryText: {
    fontSize: 12,
    color: '#fff',
    textAlign: 'center'
  },
  inputGroup: {
    width: 300
  },
  checkBox: {
||||||| merged common ancestors
  checkBoxStyle: {
=======
  secondaryText: {
    fontSize: 12,
    color: '#fff',
    textAlign: 'center'
  },
  inputGroupStyle: {
    width: 300
  },
  checkBoxStyle: {
>>>>>>> e4dca620ee9e714d44941a051d2df5cf5c909469
    backgroundColor: '#3FB8B8',
    borderWidth: 0,
    paddingVertical: 0
  }
});

export default SlideFour;
