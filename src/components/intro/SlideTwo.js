import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import { FormLabel, FormInput, CheckBox } from 'react-native-elements';

const SlideTwo = () => {
  return (
    <View style={styles.container}>

      <View style={[styles.wrapper, { flex: 0.5, paddingHorizontal: 15 }]}>
        <Text style={styles.primaryText}>
          Empieza proporcionando la siguiente información:
        </Text>
      </View>

      <View style={[styles.wrapper, { flex: 3 }]}>
        <View style={styles.inputGroup}>
          <FormLabel labelStyle={styles.labelStyle}>
            Edad (años)
          </FormLabel>
          <FormInput onChangeText={() => {}}
            inputStyle={styles.inputStyle}
            underlineColorAndroid='#fff' />
        </View>

        <View style={styles.inputGroup}>
          <FormLabel labelStyle={styles.labelStyle}>
            Peso (kg)
          </FormLabel>
          <FormInput onChangeText={() => {}}
            inputStyle={styles.inputStyle}
            underlineColorAndroid='#fff' />
        </View>

        <View style={styles.inputGroup}>
          <FormLabel labelStyle={styles.labelStyle}>
            Estatura (cm)
          </FormLabel>
          <FormInput onChangeText={() => {}}
            inputStyle={styles.inputStyle}
            underlineColorAndroid='#fff' />
        </View>

        <View style={styles.inputGroup}>
          <FormLabel labelStyle={styles.labelStyle}>
            Sexo
          </FormLabel>
          <CheckBox center
            title="Masculino"
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checked={true}
            checkedColor="#774DC6"
            uncheckedColor="#fff"
            containerStyle={styles.checkBoxStyle}
            textStyle={{ color: '#fff' }} />
          <CheckBox center
            title="Femenino"
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checked={false}
            checkedColor="#774DC6"
            uncheckedColor="#fff"
            containerStyle={styles.checkBoxStyle}
            textStyle={{ color: '#fff' }} />
        </View>
      </View>
<<<<<<< HEAD
      {/* justifyContent: 'flex-start', looks good in emulator but not in actual device */}
      <View style={[styles.wrapper, { flex: 0.5 }]}>
        <Text style={styles.secondaryText}>
          Desliza para continuar.
        </Text>
      </View>

    </View>
  );
};
||||||| merged common ancestors
    );
  }
}
=======

      <View style={[styles.wrapper, { flex: 0.5, justifyContent: 'flex-start' }]}>
        <Text style={styles.secondaryText}>
          Desliza para continuar.
        </Text>
      </View>

    </View>
  );
};
>>>>>>> e4dca620ee9e714d44941a051d2df5cf5c909469

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3FB8B8'
  },
  wrapper: {
<<<<<<< HEAD
    justifyContent: 'space-around',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#fff'
  },
  primaryText: {
||||||| merged common ancestors
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputGroupStyle: {
    width: 300
  },
  textStyle: {
=======
    justifyContent: 'space-around',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#fff'
  },
  primaryText: {
>>>>>>> e4dca620ee9e714d44941a051d2df5cf5c909469
    fontSize: 16,
    color: '#fff',
    textAlign: 'center'
  },
  secondaryText: {
    fontSize: 12,
    color: '#fff',
    textAlign: 'center'
  },
  inputGroup: {
    width: 300
  },
  labelStyle: {
    color: '#fff'
  },
  inputStyle: {
    color: '#fff'
  },
  checkBoxStyle: {
    backgroundColor: '#3FB8B8',
    borderWidth: 0,
    paddingVertical: 0
  }
});

export default SlideTwo;
