import React, { Component } from 'react';
import {
  StyleSheet,
  ListView,
  View,
  Text
} from 'react-native';
import ListItem from './ListItem';

const activityLevels = [
  {
    level: "Muy leve",
    description: `Actividades en posición sentada y de pie: pintar, manjear
    trabajo de laboratorio, computación, coser, planchar, cocinar, juegos de
    mesa, tocar un instrumento musical, etc.`
  },
  {
    level: "Leve",
    description: `Caminar, trabajos eléctricos, trabajo en restaurante,
    limpieza de casa, cuidado de los niños, golf, tenis de mesa, etc.`
  },
  {
    level: "Moderada",
    description: `Caminar vigorosamente, cortar el pasto, ciclismo en
    superficie plana, tenis, llevar una carga, etc.`
  },
  {
    level: "Intensa",
    description: `Caminar con carga pendiente hacia arriba, tala de
    árboles, excavación manual intensa, básquetbol, escalar, futbol,
    correr, natación, ciclismo con pendiente, aerobics, etc.`
  },
  {
    level: "Excepcional",
    description: `Atletas de alto rendimiento.`
  }
];

class SlideThree extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.dataSource = ds.cloneWithRows(activityLevels);
  }

  renderRow(activityLevel) {
    return <ListItem activityLevel={activityLevel} />;
  }

  render() {
    return (
      <View style={styles.container}>

        <View style={[styles.wrapper, { flex: 0.5, paddingHorizontal: 15 }]}>
          <Text style={styles.primaryText}>
            Ahora elige tu nivel de actividad física:
          </Text>
        </View>

        <View style={[styles.wrapper, { flex: 3 }]}>
          <ListView dataSource={this.dataSource}
                    renderRow={this.renderRow}
                    contentContainerStyle={{ width: 300 }} />
        </View>
<<<<<<< HEAD
        {/* justifyContent: 'flex-start', looks good in emulator but not in actual device */}
        <View style={[styles.wrapper, { flex: 0.5 }]}>
          <Text style={styles.secondaryText}>
            Desliza para continuar.
          </Text>
||||||| merged common ancestors

        <View style={{ flex: .1 }}>

=======

        <View style={[styles.wrapper, { flex: 0.5, justifyContent: 'flex-start' }]}>
          <Text style={styles.secondaryText}>
            Desliza para continuar.
          </Text>
>>>>>>> e4dca620ee9e714d44941a051d2df5cf5c909469
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3FB8B8'
  },
  wrapper: {
    justifyContent: 'space-around',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#fff'
  },
  primaryText: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center'
  },
  secondaryText: {
    fontSize: 12,
    color: '#fff',
    textAlign: 'center'
  },
});

export default SlideThree;
