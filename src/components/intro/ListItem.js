import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Card, CardSection } from '../common';

class ListItem extends Component {
  render() {
    const { level, description } = this.props.activityLevel;

    return (
      <Card>
        <CardSection style={styles.bkgStyle}>
          <Text style={styles.titleStyle}>
            {level}
          </Text>
        </CardSection>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  bkgStyle: {
    backgroundColor: '#3FB8B8'
  },
  titleStyle: {
    fontSize: 18,
    paddingLeft: 15,
    flex: 1,
    color: '#fff'
  },
  textStyle: {
    fontSize: 16,
    color: '#fff'
  }
});

export default ListItem;
