import React from 'react';
import Swiper from 'react-native-swiper';
import SlideOne from './SlideOne';
import SlideTwo from './SlideTwo';
import SlideThree from './SlideThree';
import SlideFour from './SlideFour';

const Intro = () => {
  return (
    <Swiper showButtons
      loop={false}>

      <SlideOne />

      <SlideTwo />

      <SlideThree />

      <SlideFour />

    </Swiper>
  );
};



export default Intro;
