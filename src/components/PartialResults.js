import React from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button } from 'react-native-elements';

const PartialResults = () => {
  return (
    <View style={styles.container}>

      <View style={[styles.wrapper, { flex: 0.5 }]}>
        <Text style={styles.titleStyle}>IMC y Peso Ideal</Text>
      </View>

      <View style={[styles.wrapper, { flex: 1, flexDirection: 'row', paddingHorizontal: 15 }]}>
        <View style={{ flex: 1, backgroundColor: '#00B0FF', height: 60 }}>

        </View>

        <View style={{ flex: 1, backgroundColor: '#00B200', height: 60 }}>

        </View>

        <View style={{ flex: 1, backgroundColor: '#430280', height: 60 }}>

        </View>

        <View style={{ flex: 1, backgroundColor: '#F4B800', height: 60 }}>

        </View>

        <View style={{ flex: 1, backgroundColor: '#FF7900', height: 60 }}>

        </View>

        <View style={{ flex: 1, backgroundColor: '#E50405', height: 60 }}>

        </View>
      </View>

      <View style={[styles.wrapper, { flex: 2, paddingHorizontal: 15 }]}>
        <Text style={styles.primaryText}>IMC = 30.68 kg/m2</Text>
        <Text style={styles.primaryText}>OBESIDAD GRADO I (30 - 34.9)</Text>
        <Text style={styles.primaryText}>
          Riesgo de desarrollar enfermadades cardiovasculares.
          Visita a un especialista, apégate al plan de alimentación,
          ejercicios e indicaciones. Recuerda, restablecer tu salud
          require de un compromiso.
        </Text>
        <Text style={styles.primaryText}>INTERVALO DE PESO IDEAL</Text>
        <Text style={styles.primaryText}>63.32 kg - 85.53 kg</Text>
      </View>

      <View style={[styles.wrapper, { flex: 0.75 }]}>
        <Button small
                raised
                title="Continuar"
                backgroundColor="#774DC6"
                onPress={() => Actions.goal()} />
      </View>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3FB8B8'
  },
  wrapper: {
    justifyContent: 'space-around',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#fff'
  },
  titleStyle: {
    fontSize: 22,
    color: '#fff'
  },
  primaryText: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center'
  }
});

export default PartialResults;
