import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button, CheckBox } from 'react-native-elements';


const Goal = () => {
  return (
    <View style={styles.container}>

      <View style={[styles.wrapper, { flex: 0.5, paddingHorizontal: 15 }]}>
        <Text style={styles.primaryText}>
          De acuerdo a los resultados anteriores establece una meta:
        </Text>
      </View>

      <View style={[styles.wrapper, { flex: 3 }]}>
        <View style={styles.inputGroup}>
          <CheckBox center
                    title="Bajar de peso"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checked={true}
                    checkedColor="#774DC6"
                    uncheckedColor="#fff"
                    containerStyle={styles.checkBox}
                    textStyle={{ color: '#fff' }} />
          <CheckBox center
                    title="Mantener mi peso"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checked={false}
                    checkedColor="#774DC6"
                    uncheckedColor="#fff"
                    containerStyle={styles.checkBox}
                    textStyle={{ color: '#fff' }} />
          <CheckBox center
                    title="Subir de peso"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o"
                    checked={false}
                    checkedColor="#774DC6"
                    uncheckedColor="#fff"
                    containerStyle={styles.checkBox}
                    textStyle={{ color: '#fff' }} />
        </View>
      </View>

      <View style={[styles.wrapper, { flex: 0.75 }]}>
        <Button small
                raised
                title="Continuar"
                backgroundColor="#774DC6"
                onPress={() => Actions.root()} />
      </View>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3FB8B8'
  },
  wrapper: {
    justifyContent: 'space-around',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#fff'
  },
  primaryText: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center'
  },
  inputGroup: {
    width: 300
  },
  checkBox: {
    backgroundColor: '#3FB8B8',
    borderWidth: 0,
    paddingVertical: 0
  }
});

export default Goal;
