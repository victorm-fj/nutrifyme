import React, { Component } from 'react';
import { Provider } from 'react-redux';
import firebase from 'firebase';
import store from './store';
import Router from './Router';

class App extends Component {
  componentDidMount() {
    const config = {
      apiKey: "AIzaSyBeN4_3vciriohHDxYSuKXhBAjqOzUf8S0",
      authDomain: "nutrifyme-87eb1.firebaseapp.com",
      databaseURL: "https://nutrifyme-87eb1.firebaseio.com",
      storageBucket: "nutrifyme-87eb1.appspot.com",
      messagingSenderId: "142103683929"
    };
    firebase.initializeApp(config);
  }

  render() {
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
